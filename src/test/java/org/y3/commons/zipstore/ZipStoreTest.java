package org.y3.commons.zipstore;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Christian.Rybotycky
 */
public class ZipStoreTest {

    private ZipStore instance;
    private Path sourceFile;
    final String UNIT_TEST__PATH = System.getProperty("user.dir").replace("\\", "/") + "/";
    final String UNIT_TEST__ZIPSTORE__NAME = "unit-test-zipstore.zip";
    final String UNIT_TEST__SOURCE_FILE__NAME = "unit-test-source-file.txt";
    final String UNIT_TEST__DATABASE_FILE__NAME = "unit-test-database-file";
    private boolean deleteZipStore = true;

    public ZipStoreTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.out.println("UNIT_TEST__PATH: " + UNIT_TEST__PATH);
        System.out.println("UNIT_TEST__SOURCE_FILE__NAME: " + UNIT_TEST__SOURCE_FILE__NAME);
        System.out.println("UNIT_TEST__DATABASE_FILE__NAME: " + UNIT_TEST__DATABASE_FILE__NAME);
        System.out.println("UNIT_TEST__ZIPSTORE__NAME: " + UNIT_TEST__ZIPSTORE__NAME);
        try {
            instance = new ZipStore(UNIT_TEST__PATH + UNIT_TEST__ZIPSTORE__NAME);
            FileWriter sourceWriter = new FileWriter(new File(UNIT_TEST__PATH + UNIT_TEST__SOURCE_FILE__NAME));
            sourceWriter.append(UNIT_TEST__SOURCE_FILE__NAME);
            sourceWriter.flush();
            sourceWriter.close();
            sourceFile = Paths.get(UNIT_TEST__PATH + UNIT_TEST__SOURCE_FILE__NAME);
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(ZipStoreTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @After
    public void tearDown() throws IOException {
        try {
            if (deleteZipStore) {
                Path unitTestZipStore_file = Paths.get(URI.create("file:/" + UNIT_TEST__PATH + UNIT_TEST__ZIPSTORE__NAME));
                Files.deleteIfExists(unitTestZipStore_file);
            }
            Files.deleteIfExists(sourceFile);
        } catch (IOException ex) {
            Logger.getLogger(ZipStoreTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of add method, of class ZipStore.
     * @throws java.lang.Exception
     */
    @Test
    public void testAdd() throws Exception {
        System.out.println("test: add");
        String wantedFilePathInZipStore = "/" + sourceFile.getFileName().toString();
        String expResult = wantedFilePathInZipStore;
        Path result = instance.add(sourceFile, wantedFilePathInZipStore);
        instance.close();
        Assert.assertEquals(expResult, result.toString());
    }

    /**
     * Test of add method, of class ZipStore.
     * @throws java.lang.Exception
     */
    @Test
    public void testAdd1() throws Exception {
        System.out.println("test: add1");
        String wantedFilePathInZipStore = sourceFile.getFileName().toString();
        String expResult = wantedFilePathInZipStore;
        Path result = instance.add(sourceFile, wantedFilePathInZipStore);
        instance.close();
        Assert.assertEquals("/" + expResult, result.toString());
    }

    /**
     * Test of add method, of class ZipStore.
     * @throws java.lang.Exception
     */
    @Test
    public void testAdd2Delete() throws Exception {
        System.out.println("test: add2");
        String wantedFilePathInZipStore = sourceFile.getFileName().toString();
        Path result = instance.add(sourceFile, wantedFilePathInZipStore);
        boolean fileExists = Files.exists(result);
        Assert.assertEquals(true, fileExists);
        boolean deleted = Files.deleteIfExists(result);
        Assert.assertEquals(true, deleted);
        fileExists = Files.exists(result);
        Assert.assertEquals(false, fileExists);
        instance.close();
    }

    /**
     * Test of add method, of class ZipStore.
     * @throws java.lang.Exception
     */
    @Test
    public void testAdd3Persistance() throws Exception {
        System.out.println("test: add3");
        String wantedFilePathInZipStore = sourceFile.getFileName().toString();
        Path result = instance.add(sourceFile, wantedFilePathInZipStore);
        Stream<String> lines = Files.lines(result);
        long expected = lines.count();
        instance.close();
        instance = new ZipStore(UNIT_TEST__PATH + UNIT_TEST__ZIPSTORE__NAME);
        result = instance.getFileSystem().getPath(result.toString());
        lines = Files.lines(result);
        Assert.assertEquals(expected, lines.count());
        instance.close();
    }

    /**
     * Test of accessReadableDatabase method, of class ZipStore.
     * @throws java.lang.Exception
     */
    //setup for this test case not implemented yet. case to test:
    //apache derby database is available in the zip folder
    //note: writeable access to zip-stored databases is not yet available in java,
    //because of concurrent access of the files
//    @Test
//    public void testAccessReadableDatabase() throws Exception {
//        System.out.println("test: provide database");
//        deleteZipStore = true;
//        Connection databaseConnection = instance.accessReadableDatabase(UNIT_TEST__DATABASE_FILE__NAME);
//        Assert.assertNotNull(databaseConnection);
//        instance.close();
//    }
}
