package org.y3.commons.zipstore;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright: 2017
 * Company: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ZipStore {
    
    private final FileSystem zipFileSystem;
    private URI zipFile_uri;
    private String zipFilePath;

    public ZipStore(URI zipFileUri) throws IOException {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        zipFileSystem = FileSystems.newFileSystem(zipFileUri, env);
    }
    
    public ZipStore(String _zipFilePath) throws IOException, URISyntaxException {
        zipFilePath = _zipFilePath;
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        if (!_zipFilePath.startsWith("/")) {
            _zipFilePath = "/" + _zipFilePath;
        }
        zipFile_uri = URI.create("jar:file:" + _zipFilePath);
        zipFileSystem = FileSystems.newFileSystem(zipFile_uri, env);
    }
    
    public String getPath() {
        return zipFile_uri.getPath();
    }
    
    public Path add(Path externalFile, String wantedFilePathInZipStore) throws IOException {
        if (!wantedFilePathInZipStore.startsWith("/")) {
            wantedFilePathInZipStore = "/" + wantedFilePathInZipStore;
        }
        Path pathInZipStore = zipFileSystem.getPath(wantedFilePathInZipStore);
        return Files.copy(externalFile, pathInZipStore, StandardCopyOption.REPLACE_EXISTING);
    }
    
    public FileSystem getFileSystem() {
        return zipFileSystem;
    }
    
    public void close() throws IOException {
        zipFileSystem.close();
    }
    
    public Connection accessReadableDatabase(String url) throws SQLException {
        String databaseUrl = "jdbc:derby:jar:(" + zipFilePath + ")" + url;
        return DriverManager.getConnection(databaseUrl);
    }

}
